document.addEventListener('DOMContentLoaded', () => {

    const diets = document.querySelectorAll('.main__diet');

    //set active diet
    for (let i = 0; i < diets.length; i++) {
        diets[i].addEventListener('click', function(e) {
            let data = e.target.parentNode.parentNode.getAttribute('data-diet');
            let items = document.querySelectorAll(`[data-diet=${data}]`);

            //remove active class from all elements
            [].forEach.call(diets, function(el) {
                el.classList.remove('active');
            });

            //add active class to elements with correct data attribute
            [].forEach.call(items, function(el) {
                el.classList.add('active');
            });

            //TODO display choice of meals and pricing
        });
    }

    //init diets slider
    new Splide('.pricing__slider', {
        type: 'loop',
        perPage: 1,
        autoplay: true,
        gap: '1rem',
        interval: 5000,
        pagination: false
    }).mount();


});
