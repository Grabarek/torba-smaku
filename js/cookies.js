document.addEventListener("DOMContentLoaded", () => {

    const cookiesButton = document.querySelector('.cookies__button');
    const cookiesWindow = document.querySelector('.cookies');

    const cookieName = 'cookie_accept';
    const setCookie = (cname, cvalue, exdays) => {
        let d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };

    // if cookie doesn't exist - show warning window
    if(document.cookie.indexOf(cookieName) === -1) {
        cookiesWindow.classList.add('active');
    }

    // create cookie for 30 days on button click and hide warning window
    cookiesButton.addEventListener("click", () => {
        setCookie(cookieName, true, 30);
        cookiesWindow.classList.remove('active');
    }, false);
});
