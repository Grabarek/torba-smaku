document.addEventListener("DOMContentLoaded", () => {

    const modal = document.querySelector('.contact .contact__modal');
    const modalClose = document.querySelector('.contact .modal__close');
    const form = document.querySelector('.contact .contact__form');

    //TODO form submit
    form.addEventListener('submit', e => {
        e.preventDefault();

        //success
        modal.classList.add('active');
    });

    // hide modal on close click
    modalClose.addEventListener("click", () => {
        modal.classList.remove('active');
    }, false);
});
