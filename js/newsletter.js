document.addEventListener("DOMContentLoaded", () => {

    const modal = document.querySelector('.newsletter .newsletter__modal');
    const emailInput = document.querySelector('.newsletter #newsletter_email');
    const modalClose = document.querySelector('.newsletter .modal__close');
    const form = document.querySelector('.newsletter .newsletter__form');

    //TODO form submit
    form.addEventListener('submit', e => {
        e.preventDefault();

        const email = emailInput.value;
        console.log(email);

        //success
        modal.classList.add('active');
    });

    // hide modal on close click
    modalClose.addEventListener("click", () => {
        modal.classList.remove('active');
    }, false);
});
