document.addEventListener("DOMContentLoaded", () => {

    const menuOpen = document.querySelector('.nav__open');
    const menuClose = document.querySelector('.nav__close');
    const navMobile = document.querySelector('.nav__mobile');

    //open menu
    menuOpen.addEventListener("click", () => {
        navMobile.classList.add('active');
    }, false);

    //close menu
    menuClose.addEventListener("click", () => {
        navMobile.classList.remove('active');
    }, false);

});
