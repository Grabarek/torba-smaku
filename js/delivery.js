document.addEventListener("DOMContentLoaded", () => {

    const successModal = document.querySelector('.delivery-page .delivery__modal.success');
    const failureModal = document.querySelector('.delivery-page .delivery__modal.failure');
    const postalInput = document.querySelector('.delivery-page #postal');
    const cityInput = document.querySelector('.delivery-page #city');
    const modalClose = document.querySelectorAll('.delivery-page .modal__close');
    const form = document.querySelector('.delivery-page .delivery__form-form');

    //TODO form submit and city delivery logic
    form.addEventListener('submit', e => {
        e.preventDefault();

        const postal = postalInput.value;
        const city = cityInput.value;
        console.log(postal, city);

        //success
        successModal.classList.add('active');

        //failure
        // failureModal.classList.add('active');
    });

    // hide modals on close click
    [].forEach.call(modalClose, el => {
        el.addEventListener('click', () => {
            failureModal.classList.remove('active');
            successModal.classList.remove('active');
        });
    });
});
