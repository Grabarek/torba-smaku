document.addEventListener("DOMContentLoaded", () => {


    new Splide('.banner__slider', {
        type: 'loop',
        perPage: 1,
        autoplay: true,
        interval: 5000,
        pagination: false
    }).mount();


    new Splide('.diets__slider', {
        type: 'loop',
        perPage: 1,
        autoplay: true,
        gap: '1rem',
        interval: 5000,
        pagination: false
    }).mount();


    new Splide('.testimonials__slider', {
        type: 'loop',
        perPage: 3,
        autoplay: true,
        interval: 5000,
        pagination: false,
        gap: '4rem',
        padding: '2rem',
        autoHeight: true,
        focus: 'center',
        trimSpace: false,
        breakpoints: {
            1024: {
                perPage: 1,
            },
        }
    }).mount();

});
