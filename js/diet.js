document.addEventListener('DOMContentLoaded', () => {

    const days = document.querySelectorAll('.menu__day');
    const menus = document.querySelectorAll('.menu__list');

    //switch menu according to active day
    for (let i = 0; i < days.length; i++) {
        days[i].addEventListener('click', function(e) {
            let data = e.target.getAttribute('data-day');
            let items = document.querySelectorAll(`[data-day=${data}]`);

            //remove active class from all elements
            [].forEach.call(menus, function(el) {
                el.classList.remove('active');
            });
            [].forEach.call(days, function(el) {
                el.classList.remove('active');
            });

            //add active class to elements with correct data attribute
            [].forEach.call(items, function(el) {
                el.classList.add('active');
            });
        });
    }

    //init diets slider
    new Splide('.diet__slider', {
        type: 'loop',
        perPage: 1,
        autoplay: true,
        gap: '1rem',
        interval: 5000,
        pagination: false
    }).mount();


});
